while 1:
    data = input()
    if data == "0 0":
        break

    n, m = map(int, data.split(" "))

    log = sorted(
        tuple(map(int, input().split(" ")))
        for i in range(m)
    )

    raped = {1}

    for _, a, b in log:
        if a in raped:
            raped |= {b}

    print(len(raped))