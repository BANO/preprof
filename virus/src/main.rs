use std::io::{self, BufRead};


fn main() {
    loop {
        let input = get_line();
        if input.trim() == "0 0" {
            break;
        }

        let input: Vec<i32> = input.trim().split_whitespace()
            .map(|a| a.parse().unwrap())
            .collect()
        ;
        let n = input[0];
        let m = input[1];

        let mut log = Vec::new();
        for _ in 0..m {
            let input = get_line();
            let input: Vec<i32> = input.trim().split_whitespace()
                .map(|a| a.parse().unwrap())
                .collect()
            ;
            let t = input[0];
            let a = input[1];
            let b = input[2];
            log.push((t, a, b))
        }

        log.sort();

        let mut raped = vec![1];

        for el in log {
            let a = el.1;
            let b = el.2;

            if raped.contains(&a) {
                raped.push(b)
            }
        }

        println!("{}", raped.len())
    }
}


fn get_line() -> String {
    let mut line = String::new();
    let stdin = io::stdin();
    stdin.lock().read_line(&mut line).unwrap();
    line
}


