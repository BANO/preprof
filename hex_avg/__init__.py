"""
in_hex = input()

ar = int(in_hex, 16) % 5


def combinator(base, rest=""):
    if base.startswith('0'):
        return []

    if not rest:
        return [int(base, 16) % 5]

    res = []

    for i in range(len(rest)):
        res += combinator(base + rest[i], rest[:i] + rest[i + 1:])

    return res


result = combinator(in_hex)
avg = sum(result) / len(result)

print(int(avg) if avg.is_integer() else avg)
"""

print(int(input(), 16) % 5)
