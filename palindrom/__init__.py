import math


def not_palindrom(arr):
    nn = int(math.floor(len(arr) / 2))
    n = len(arr) - 1
    for j in range(0, nn + 1):
        if arr[j] != arr[n - j]:
            return 1
    return 0


n = int(eval(input()))
inp = input().split();
A = []
for i in range(0, n):
    if len(inp) > i:
        A.append(int(inp[i]))
index = -2
value = 0
nn = int(math.floor(n / 2))
n -= 1
for i in range(0, nn):
    if A[i] != A[n - i]:
        if index == -2 and (A[i + 1] == A[n - i] or A[i] == A[n - i - 1]):
            wrong = not_palindrom(A[i:n - i])
            if wrong == 1:
                wrong = not_palindrom(A[i + 1:n - i + 1])
                if wrong == 0:
                    index = n - i + 1
                    value = A[i]
            else:
                index = i
                value = A[n - i]
            if wrong == 1:
                index = -1
                break
            else:
                A.insert(index, value)
                n = len(A) - 1
        else:
            index = -1
            break
if index == -1:
    print(-1)
else:
    if index == -2:
        index = nn
        value = A[nn]
        A.insert(index, value)
    print(index, value)
