use std::io::{self, BufRead};
use std::str::Split;

fn main() {
    let tests: u32 = get_line().trim().parse().unwrap();


    for _i in 0..tests {
        let input = get_line();

        let _input: Vec<i32> = input.trim().split_whitespace()
            .map(|a| a.parse().unwrap())
            .collect()
        ;


        let mut A: Vec<i32> = Vec::new();
        let mut Amax = 0;

        for el in get_line().trim().split_whitespace() {
            let el = el.parse().unwrap();
            A.push(el);
            if Amax < el {
                Amax = el
            }
        }
        let mut B: Vec<i32> = Vec::new();
        for el in get_line().trim().split(" ") {
            let el = el.parse().unwrap();
            if el < Amax {
                B.push(el);
            }
        }

        let mut result = 0;
        for a in &A {
            for b in &B {
                if a > b {
                    result += 1;
                }
            }
        }

        println!("{}", result);
    }
}

fn get_line() -> String {
    let mut line = String::new();
    let stdin = io::stdin();
    stdin.lock().read_line(&mut line).unwrap();
    line
}