for _ in range(int(input())):

    input()  # пропускаем строку с количеством особей

    ma = 0

    A = []
    for a in input().split(" "):
        a = int(a)
        A.append(a)
        if a > ma:
            ma = a

    B = []
    for a in input().split(" "):
        a = int(a)
        if a < ma:
            B.append(a)

    res = 0

    for b in B:
        for a in A:
            if a > b:
                res += 1

    print(res)
