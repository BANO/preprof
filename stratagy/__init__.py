n = int(input())

MAX_INT = 1 << 33 - 1
s = 1 << 32 - 1

value = 0
step = 0
while value <= s:
    value += n ** step
    step += 1

    # Exit max capability
    if value > s and value > MAX_INT:
        value %= MAX_INT

print(step)
